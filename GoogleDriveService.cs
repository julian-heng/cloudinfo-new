﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace CloudInfo
{
    public class GoogleDriveService : ICloudService
    {
        public ClientSecrets ClientTokens { get; } = new ClientSecrets
        {
            ClientId = ConfigurationManager.AppSettings["GoogleDriveClientID"],
            ClientSecret = ConfigurationManager.AppSettings["GoogleDriveClientSecret"]
        };

        public string ClientId => ConfigurationManager.AppSettings["GoogleDriveClientID"];
        public string ClientSecret => ConfigurationManager.AppSettings["GoogleDriveClientSecret"];
        public UserCredential Credential { get; set; } = null;

        public string[] Scope { get; } =
        {
            DriveService.Scope.DriveReadonly
        };

        public List<string> FileNameList { get; } = new List<string>();

        public void UpdateToken()
        {
            if (Credential == null)
                Credential = GetCredential().Result;
        }

        public async Task PopulateFileList(string path)
        {
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = Credential,
                ApplicationName = "CloudInfo"
            });

            var listRequest = service.Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name)";

            var files = listRequest.Execute().Files;
            foreach (var file in files) FileNameList.Add(file.Name);
        }

        private async Task<UserCredential> GetCredential()
        {
            return await GoogleWebAuthorizationBroker.AuthorizeAsync(ClientTokens, Scope, "User",
                CancellationToken.None, new NullDataStore());
        }
    }
}