﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;

namespace CloudInfo
{
    internal class DropBoxService : ICloudService
    {
        private readonly List<Metadata> _fileList = new List<Metadata>();

        public string ClientId => ConfigurationManager.AppSettings["DropboxClientID"];
        public string ClientToken { get; set; } = null;
        public Uri RedirectUri { get; } = new Uri("https://www.dropbox.com/1/oauth2/display_token");

        public List<string> FileNameList
        {
            get
            {
                var result = new List<string>();
                foreach (var item in _fileList)
                    result.Add(item.Name);
                return result;
            }
        }

        public void UpdateToken()
        {
            if (ClientToken == null)
                ClientToken = GetAccessToken();
        }

        public async Task PopulateFileList(string path)
        {
            Console.WriteLine(path);
            using (var db = new DropboxClient(ClientToken))
            {
                var list = await db.Files.ListFolderAsync(path);
                foreach (var file in list.Entries)
                    if (file.IsFolder)
                        PopulateFileList($"{path}/{file.Name}").Wait();
                    else if (file.IsFile)
                        _fileList.Add(file);
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()}(ApiKey = {ClientId}, ApiToken = {ClientToken})";
        }

        private string GetAccessToken()
        {
            string token = null;
            var state = Guid.NewGuid().ToString("N");
            var authUri =
                DropboxOAuth2Helper.GetAuthorizeUri(OAuthResponseType.Token, ClientId, RedirectUri, state);

            Utils.OpenBrowser(authUri.ToString());
            Console.Write("Enter Token> ");
            token = Console.ReadLine();

            return token;
        }
    }
}