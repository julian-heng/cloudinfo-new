﻿using System;
using System.Configuration;
using NLog;

namespace CloudInfo
{
    internal class Program
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            new Program().Run();
        }

        public void Run()
        {
            foreach (var i in ConfigurationManager.AppSettings.AllKeys)
                _log.Debug($"{i}: {ConfigurationManager.AppSettings[i]}");

            Menu();
        }

        public void Menu()
        {
            ICloudService service = null;
            string command;
            var exit = false;
            var back = false;

            do
            {
                back = false;
                Console.Write("Select service ([G]oogleDrive, [D]ropbox, [O]neDrive, [E]xit)> ");
                command = Console.ReadLine();

                switch (command)
                {
                    case "g":
                    case "G":
                        service = new GoogleDriveService();
                        break;

                    case "d":
                    case "D":
                        service = new DropBoxService();
                        break;

                    case "o": case "O":
                        service = new OneDriveService();
                        break;

                    case "e":
                    case "E":
                        exit = true;
                        break;

                    default:
                        Console.Error.WriteLine("Unknown input");
                        break;
                }

                if (service is null)
                {
                    Console.Error.WriteLine("Service is null");
                    continue;
                }

                if (exit)
                    continue;

                do
                {
                    service.UpdateToken();
                    Console.Write("Select command (Print [F]iles, [B]ack)> ");
                    command = Console.ReadLine();

                    switch (command)
                    {
                        case "f":
                        case "F":
                            Console.Write("Filename> ");
                            command = Console.ReadLine();
                            command = command.CompareTo("") == 0 || command.CompareTo("/") == 0
                                ? string.Empty
                                : command;
                            service.PopulateFileList(command).Wait();
                            service.FileNameList.ForEach(Console.WriteLine);
                            break;
                        case "b":
                        case "B":
                            back = true;
                            break;
                    }
                } while (!back);
            } while (!exit);
        }
    }
}