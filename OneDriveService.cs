﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

using Microsoft.Graph;
using Microsoft.OneDrive.Sdk.Authentication;
using NLog.Targets;


namespace CloudInfo
{
    public class OneDriveService : ICloudService
    {
        public string ClientId => ConfigurationManager.AppSettings["OneDriveClientID"];
        public IAuthenticationProvider AuthProvider { get; set; }

        public string[] Scope { get; } =
        {
            "onedrive.readonly"
        };

        public List<string> FileNameList { get; }

        public void UpdateToken()
        {
            if (AuthProvider == null)
                AuthProvider = GetAuthProvider().Result;
        }

        public async Task PopulateFileList(string path)
        {
            throw new NotImplementedException();
        }

        private async Task<IAuthenticationProvider> GetAuthProvider()
        {
            var helper = new OAuthHelper();
            var url = helper.GetAuthorizationCodeRequestUrl(ClientId, "https://login.microsoftonline.com/common/oauth2/nativeclient", Scope, null);
            Console.WriteLine(url);
            return null;
        }
    }
}