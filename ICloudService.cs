﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CloudInfo
{
    internal interface ICloudService
    {
        public List<string> FileNameList { get; }
        public void UpdateToken();
        public Task PopulateFileList(string path);
    }
}